// Технічні вимоги:
// У файлі index.html лежить розмітка двох полів вводу пароля.
// Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
// Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
// Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
// Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
// Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
// Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
// Після натискання на кнопку сторінка не повинна перезавантажуватись
// Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.


let openEye = document.querySelector('#show');

let openEyeConfirm = document.querySelector('#show-second');

let firstPassword = document.querySelector('#first-password');

let confirmPassword = document.querySelector('#confirm-password');


openEye.addEventListener('click', function () {
     
    this.classList.toggle('fa-eye-slash');
    const typeFirstPassword = firstPassword.getAttribute("type") === "password" ? "text" : "password"; firstPassword.setAttribute("type", typeFirstPassword);

});

openEyeConfirm.addEventListener('click', function () {
    
    this.classList.toggle('fa-eye-slash');
    const typeConfirmPassword = confirmPassword.getAttribute("type") === "password" ? "text" : "password"; confirmPassword.setAttribute("type", typeConfirmPassword);

});

let confirmBtn = document.querySelector('.btn');

confirmBtn.addEventListener('click', function () {
    
    let firstPasswordValue = document.querySelector('#first-password').value;

    let confirmPasswordValue = document.querySelector('#confirm-password').value;

    if (firstPasswordValue !== confirmPasswordValue){

        const divRedText = document.querySelector('.red-text');
        const redText = document.createElement('span');
        redText.className = 'error';
        redText.textContent = 'password do not match!';
        redText.style.color = 'red';
        
        divRedText.append(redText);

    }else if (firstPasswordValue === '' || confirmPasswordValue === ''){
        window.alert('enter your password');
    }
    else{
        window.alert('You are welcome');
    }
});

const form = document.querySelector("form");

form.addEventListener('submit', function (e) {
    e.preventDefault();
});